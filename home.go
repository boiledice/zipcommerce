package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"sort"
	"strings"
)

//StaticHomePage Will hold the Static Home Page items
type StaticHomePage struct {
	Title         string
	HeaderTitle   string
	SnipCartKey   string
	Items         map[string]Inventory //map of SKU to an inventory item
	Collections   []string             //a slice of all the collections
	ShopAbout     string
	ShopPhone     string
	ShopEmail     string
	GAnalyticsTID string
}

func createStaticRecoshinHomePage(invRef map[string]Inventory, a AdminPage) {
	staticHomeDirectory := "recoshins"
	staticIndex := staticHomeDirectory + "/" + a.ShopURL + "/index.html"
	collections := []string{}
	inCollection := false

	//create the collections directory if not there
	mkdirerr := os.MkdirAll(staticHomeDirectory, os.ModePerm)
	if mkdirerr != nil {
		fmt.Println("Err: Coud not create directory - ", staticHomeDirectory)
	}
	//loop through the list of all inventory item looking for their tags
	for _, v := range invRef {
		fmt.Println("Home page V=", v)

		//loop through each inventory itemstags
		for _, itemtag := range v.ItemTags {
			inCollection = false
			//loop through the collection to search for itemtag
			//fmt.Println("Looking for itemtag:", itemtag, "in collection:", collections)
			for _, tag := range collections {
				//check if a tag already exists in collection if not append to collection
				//fmt.Println("Comparing:", tag, " & ", itemtag)
				if strings.EqualFold(itemtag, tag) {
					//cannot be added as its found in collection
					//fmt.Println("Found ", itemtag, " in ", collections, "hence not being added")
					inCollection = true
					break
				}
			}
			if !inCollection {
				//fmt.Println("this tag:", itemtag, " was not found in the collection: ", collections, " hence being added now")
				collections = append(collections, itemtag)
				fmt.Println("New collection after append:", collections)
				inCollection = false
			}
		}
	}
	//Sort the string slices
	sort.Strings(collections)

	//fmt.Println("Home Page Collections=", collections)
	staticHome := StaticHomePage{Title: a.ShopName,
		HeaderTitle:   "",
		Items:         invRef,
		SnipCartKey:   a.SnipCartKey,
		Collections:   collections,
		ShopAbout:     a.ShopAbout,
		ShopEmail:     a.ShopEmail,
		ShopPhone:     a.ShopPhone,
		GAnalyticsTID: a.GAnalyticsTID}
	templateLocation := "templates/recoshins/" + a.ShopTemplate + "/layout/index.tmpl"

	t, err := template.ParseFiles(templateLocation)

	buff := bytes.NewBufferString("")
	if err == nil {
		t.Execute(buff, staticHome)
	}

	errr := ioutil.WriteFile(staticIndex, buff.Bytes(), 0600)
	if errr != nil {
		fmt.Println("Could not save Index.html Page")
	}

	fmt.Println("Completed: Updating/Creating Recoshin Home/Index Page!")
}
