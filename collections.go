package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"os"
	"sort"
)

//StaticHomePage Will hold the Static Home Page items
type StaticCollectionsPage struct {
	Title         string
	HeaderTitle   string
	SnipCartKey   string
	Items         map[string]Inventory //map of SKU to an inventory item
	Collections   []string             //a slice of all the collections
	Collection    string
	ShopAbout     string
	ShopPhone     string
	ShopEmail     string
	GAnalyticsTID string
}

//Gets the collections from the tags provided by client in the spreadsheet
func getInventoryCollections(invRef map[string]Inventory) (map[string][]string, error) {
	collection := map[string][]string{}
	for _, v := range invRef {
		//Collect the tags from each SKU
		fmt.Println("Tags Found:", v.ItemTags)
		//splitTags := strings.Fields(v.ItemTags)
		for _, t := range v.ItemTags {
			//fmt.Println("Appending SKU",v.ItemSKU, "to tag",t, "for collection:", collection)
			collection[t] = append(collection[t], v.ItemSKU)
		}
	}
	return collection, nil
}

//Create the Static Collection pages : each page is for a collection (tag) with all items with that tag
func createStaticRecoshinCollections(invRef map[string]Inventory, a AdminPage, collRef map[string][]string) {

	//Create Collections slice for Collections Page
	var collections []string
	for k, _ := range collRef {
		collections = append(collections, k)
	}

	//Sort the string slices
	sort.Strings(collections)

	fmt.Println("List of collections to be used in static collectiosn page:", collections)

	//loop through the collections and for each collection create a static collection page
	for k, v := range collRef {
		CollectionInvRef := map[string]Inventory{} //create a temporary list of invetory for each SKU in the colelction
		//print collection and SKUs under each collection
		fmt.Println("\n\nStart creating Collections pages for collection:", k, "with SKUS:", v)
		for _, j := range v {
			CollectionInvRef[j] = invRef[j]
		}

		//loop through the SKUs from each collection and create a items colelctionInvRef object for the page
		fmt.Print("Inventories for Collection:", k, " with items map:", CollectionInvRef)

		staticCollectionPage := StaticCollectionsPage{Title: a.ShopName,
			HeaderTitle:   "",
			Items:         CollectionInvRef,
			SnipCartKey:   a.SnipCartKey,
			Collections:   collections,
			Collection:    k,
			ShopEmail:     a.ShopEmail,
			ShopAbout:     a.ShopAbout,
			ShopPhone:     a.ShopPhone,
			GAnalyticsTID: a.GAnalyticsTID}
		fmt.Println("\nCreating Static page:", staticCollectionPage)
		go createAsyncCollectionsPage(staticCollectionPage, &a)
	}
}

func createAsyncCollectionsPage(staticCollectionPage StaticCollectionsPage, a *AdminPage) {
	staticDirectory := "recoshins/" + a.ShopURL + "/collections/"

	//create the collections directory if not there
	mkdirerr := os.MkdirAll(staticDirectory, os.ModePerm)
	if mkdirerr != nil {
		fmt.Println("Err: Coud not create directory - ", staticDirectory)
	}
	fileName := staticDirectory + staticCollectionPage.Collection + ".html"
	fmt.Println(fileName)
	templateLocation := "templates/recoshins/" + a.ShopTemplate + "/layout/collections.tmpl"
	tmpl, err := template.ParseFiles(templateLocation)

	buff := bytes.NewBufferString("")
	if err == nil {
		tmpl.Execute(buff, staticCollectionPage)
	}
	//fmt.Println(buff)
	err = ioutil.WriteFile(fileName, buff.Bytes(), 0666)
	if err != nil {
		fmt.Println("Could not save", fileName, " Page")
	}

}
