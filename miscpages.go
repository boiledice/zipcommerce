package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
)

//aboutPage
type AboutPage struct {
	Title       string
	HeaderTitle string
	MainText    string
}

func createAboutPage(a AdminPage) {
	staticDirectory := "recoshins/" + a.ShopURL + "/"
	fileName := staticDirectory + "about.html"
	templateLocation := "templates/recoshins/" + a.ShopTemplate + "/layout/about.tmpl"
	tmpl, err := template.ParseFiles(templateLocation)

	pageObject := AboutPage{Title: a.ShopName,
		HeaderTitle: "About Us",
		MainText:    a.ShopAbout}

	buff := bytes.NewBufferString("")
	if err == nil {
		tmpl.Execute(buff, pageObject)
	}
	fmt.Println(buff)
	err = ioutil.WriteFile(fileName, buff.Bytes(), 0666)
	if err != nil {
		fmt.Println("Could not save", fileName, " Page")
	}

}
