package main

import (
	"bytes"
	"fmt"
	"html/template"
	"io/ioutil"
	"sort"
)

//StaticPDPPage will hold the Static PDP page details or items
type StaticPDPPage struct {
	Title         string
	HeaderTitle   string
	SnipCartKey   string
	ShopAbout     string
	ShopPhone     string
	ShopEmail     string
	Item          Inventory
	Collections   []string
	GAnalyticsTID string
}

func createStaticRecoshinPDP(invRef map[string]Inventory, a AdminPage, collRef map[string][]string) {
	//Looping through each SKU in the Item Inventory Map "im" and create itempdp1.html
	fmt.Println("STARTED: Updating/Creating Recoshin PDP Pages!")
	fmt.Print("Creating pages... ")

	//Create Collections slice for Collections Page
	var collections []string
	for k, _ := range collRef {
		collections = append(collections, k)
	}

	//Sort the string slices
	sort.Strings(collections)

	//Channel to communicate fileName created
	//fileNameChann := make(chan string)
	//Size of inventory map
	size := len(invRef)
	s := make([]SearchIndex, size)
	counter := 0
	for _, v := range invRef {
		staticPdpPage := StaticPDPPage{Title: v.ItemName,
			HeaderTitle:   a.ShopName,
			SnipCartKey:   a.SnipCartKey,
			ShopPhone:     a.ShopPhone,
			ShopAbout:     a.ShopAbout,
			ShopEmail:     a.ShopEmail,
			Item:          v,
			Collections:   collections,
			GAnalyticsTID: a.GAnalyticsTID}
		go createAsyncPDPPages(staticPdpPage, &a)
		fileName := "./" + v.ItemSKU + ".html"
		fmt.Println("FileName generated ", fileName)
		//fmt.Println("Other data ", v.ItemName)
		pageIndex := SearchIndex{Id: counter + 1, Title: v.ItemName, Href: fileName, Content: v.ItemDesc, tag: v.ItemName}
		s[counter] = pageIndex
		counter = counter + 1
	}

	fmt.Println("Completed: Updating/Creating Recoshin PDP Pages!")

	createLunrIndex(&s, a.ShopURL)
	fmt.Println("Completed : Lunr index generation")

}

func createAsyncPDPPages(staticPdpPage StaticPDPPage, a *AdminPage) {
	staticDirectory := "recoshins/" + a.ShopURL + "/"
	fileName := staticDirectory + staticPdpPage.Item.ItemSKU + ".html"
	//fmt.Println(fileName)
	templateLocation := "templates/recoshins/" + a.ShopTemplate + "/layout/pdp.tmpl"
	tmpl, err := template.ParseFiles(templateLocation)
	fmt.Println("Print staticPDPPage values:", staticPdpPage)

	buff := bytes.NewBufferString("")
	if err == nil {
		tmpl.Execute(buff, staticPdpPage)
	}
	//fmt.Println(buff)
	err = ioutil.WriteFile(fileName, buff.Bytes(), 0666)
	if err != nil {
		fmt.Println("Could not save", fileName, " Page")
	}

}
