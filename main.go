package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	_ "net/http/pprof"
	"os"
	"strings"
	"time"

	"github.com/termie/go-shutil"
	//"github.com/stackimpact/stackimpact-go"
)

const (
	SHOP_OFF     = 0
	SHOP_ON      = 1
	SHOP_ON_DEMO = 3
	SHOP_EXPIRED = 4
)

func init() {
	//check if temlpates folder is setup with two sub folders: [if NOT then create the folders]
	//	private: which is used to hold all templates for recoshin and are private to the app
	//	recoshins: or public which is used to hold all templates which can be used by users
	dir := "templates/private/"
	mkdirerr := os.MkdirAll(dir, os.ModePerm)
	if mkdirerr != nil {
		fmt.Println("Err: Coud not create directory - ", dir)
	}

	dir = "templates/recoshins/"
	mkdirerr = os.MkdirAll(dir, os.ModePerm)
	if mkdirerr != nil {
		fmt.Println("Err: Coud not create directory - ", dir)
	}

	//check if the static assets folder is setup [if NOT then create the folder]
	//	static folder holds all the static assets required by recoshin app and has to be generally public
	dir = "static"
	mkdirerr = os.MkdirAll(dir, os.ModePerm)
	if mkdirerr != nil {
		fmt.Println("Err: Coud not create directory - ", dir)
	}

	//Check if "recoshins" folder is setup: [if not then create it]
	//	recoshins: or children of recoshin is the folder which will host the actual output of the engine
	dir = "recoshins"
	mkdirerr = os.MkdirAll(dir, os.ModePerm)
	if mkdirerr != nil {
		fmt.Println("Err: Coud not create directory - ", dir)
	}

	//We should also check and bootstrap the app to download the basic admin.tmpl and one theme template with the app
	//we can download the themes and templates from gitlab repository

}

//Search index struct
type SearchIndex struct {
	Id      int
	Title   string
	Href    string
	Content string
	tag     string
}

func createStaticRecoshin(invURL string, a AdminPage) error {
	start := time.Now()

	//First create the directory or folder
	shopDir := "recoshins/" + a.ShopURL
	err := os.MkdirAll(shopDir, 0711)
	if err != nil {
		fmt.Println("Could not create recoshins directory to save pages!")
		return err
	}

	var collRef map[string][]string
	//read sheet data and load
	//read img URI and get data and load --- not required now as we will get the image URLs from the sheet
	//invURL = "https://docs.google.com/spreadsheets/d/1DV-dDZ9o6zO6HGOTzojX25LjLYcrLhgvTSd9wkgAF1I/pub?gid=1261646539&single=true&output=csv"
	invRef, err := getInventory(invURL)
	if err != nil {
		fmt.Print("Error from getInventory: ", err)
		return err
	} else {
		//fmt.Println("Successfully Parsed Published GSheet and Created the Inventory Map:", invRef, "\n")
		//now retrieve the collections from the inventory  dataset
		collRef, err = getInventoryCollections(invRef)
		if err != nil {
			fmt.Print("Error from getInventoryCollections: ", err)
			return err
		} else {
			fmt.Println("Successfully Parsed Inventory and Created the Collections (collRef) map: ", collRef)
		}

		//Commented the below line as template will be selected by the user - Ravi
		//a.ShopTemplate = "eshopper" //"simple"

		//==================go create static pages===========================//
		//move template static assets to shopDir
		go moveStaticTemplateAssets(shopDir, a.ShopTemplate)

		//go create Collections pages
		go createStaticRecoshinCollections(invRef, a, collRef)

		//go create Home page
		go createStaticRecoshinHomePage(invRef, a)

		//go create PDP pages
		go createStaticRecoshinPDP(invRef, a, collRef)

	}

	elapsed := time.Since(start)
	fmt.Println("Time taken to create Static pages:", elapsed)
	return nil
}

func createLunrIndex(data *[]SearchIndex, shopURL string) {
	fmt.Println("START : Started creating LunrIndex file")
	fileName := "recoshins/" + shopURL + "/data.json"

	datajsonfile, err := os.Create(fileName)
	if err != nil {
		fmt.Println("Error occurred: ", err)
	}

	defer datajsonfile.Close()

	datajson, err := json.Marshal(data)

	if err != nil {
		fmt.Println("Error occurred while json encoding : ", err)
	}

	d1, err := datajsonfile.Write(datajson)

	if err != nil {
		panic(err)
	}
	fmt.Printf("wrote %d bytes \n", d1)

}

//TODO:: Move template static assets to the generated shop folder
func moveStaticTemplateAssets(shopURL string, template string) {
	fmt.Println("Move all static assets from ", template, "to ", shopURL)

	//first get all the directories from the template directory
	static, _ := ioutil.ReadDir("./templates/recoshins/" + template + "/static")
	fmt.Println("Shop Static contents:", static)

	//second get all files from the template directory

	//Create all the directories in root

	//Copy all the files from across all directories in app template folder to shop Directory
	//srcPath := "templates/recoshins/" + template + "/static/"
	//dstPath := shopURL + "/"
	//fmt.Println("Source Path:", srcPath, "\n Destination Path:", dstPath)
	//err := shutil.CopyTree(srcPath, dstPath, nil)
	//if err != nil {
	//	fmt.Println(">>>Failed to copy static assets from:", template, " to shop", shopURL)
	//}

	for _, v := range static {
		srcPath := "templates/recoshins/" + template + "/static/" + v.Name()
		dstPath := shopURL + "/" + v.Name()
		fmt.Println("Source Path:", srcPath, "\n Destination Path:", dstPath)
		err := shutil.CopyTree(srcPath, dstPath, nil)
		if err != nil {
			fmt.Println(">>>Failed to copy static assets from:", v.Name(), " to shop", shopURL)
		}
	}

}

func stripchars(str, chr string) string {
	return strings.Map(func(r rune) rune {
		if strings.IndexRune(chr, r) < 0 {
			return r
		}
		return -1
	}, str)
}

func main() {
	// Only setup to profile the application performance and can be removed
	// -please let me know I will invite you to view the stackimpact dashboard
	//agent := stackimpact.NewAgent()
	//agent.Configure("b0ffba5b86310eebd23a7e506ab84769268010ab", "recoshin")
	//----------------Profiling code ends-----------------------------------
	portPtr := flag.String("port", "8080", " Change default port from 8080")
	flag.Parse()
	hostPort := ":" + *portPtr
	fmt.Println("Args passed:", hostPort)

	http.HandleFunc("/admin", adminHandler)
	http.Handle("/recoshins/", http.StripPrefix("/recoshins", http.FileServer(http.Dir("./recoshins"))))
	http.Handle("/static/", http.StripPrefix("/static", http.FileServer(http.Dir("./static"))))
	http.HandleFunc("/save", submitHandler)
	http.HandleFunc("/", launchHandler)
	log.Println(http.ListenAndServe(hostPort, nil))
}
