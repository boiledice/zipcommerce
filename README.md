**zipcommerce** is the fastest ecommerce website builder.
The platform manages your data on google spreadsheets and also hosts your website.

# Build instructions:
1. Open _.gitlab-ci.yml_ and 
2. Read _compile-go-1.7_
3. Follow the same steps as in that stage. _ignore the ones not needed_

# To run locally:
1. _go run *.go_

# Note:
1. The default port for the application is _:8080_
2. Override the default port by using the -port param
    *  e.g. 
        *  _go run *.go -port 8081_
        *  _./\<recoshin binary\> -port 80_
