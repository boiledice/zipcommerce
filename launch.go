package main

import (
	// "encoding/json"
	// "fmt"
	"html/template"
	// "io/ioutil"
	"net/http"
	// "os"
	// "time"
	// "github.com/gorilla/securecookie"
)

func launchHandler(w http.ResponseWriter, r *http.Request) {

	//display launch page
	t, _ := template.ParseFiles("templates/private/layout/launch.tmpl")
	t.Execute(w, nil)
}
