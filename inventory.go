package main

import (
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"net/http"
	"reflect"
	"regexp"
	"strconv"
	"strings"

	"google.golang.org/api/sheets/v4"
)

//Inventory will hold the inventory that will be used to create the static pages.
//The inventory wil be read from some cloud service which will host the inventory
type Inventory struct {
	ItemSKU      string   //0
	ItemUPC      uint64   //1
	ItemName     string   //2
	ItemDesc     string   //3
	ItemType     string   //4
	ItemQuantity int      //5
	ItemPrice    float32  //6
	ItemShipping float32  //7
	ItemImage    string   //8
	ItemTags     []string //9: tags are a slice of strings
}

// See function specification in main comment body above
func getInventory(invPath string) (map[string]Inventory, error) {
	var Inv map[string]Inventory
	var err error
	var invSource string

	//If CSV found in URI set invSource to csv
	if strings.Contains(invPath, "csv") || strings.Contains(invPath, "CSV") {
		invSource = "csv"
	} else {
		invSource = "gSheet"
	}
	switch invSource {
	case "csv":
		Inv, err = getInventoryFromCSV(invPath)
	case "gSheet":
		Inv, err = getInventoryFromGSheet(invPath)
	default:
		return nil, err
	}

	return Inv, nil //Inv is a map of keys of type string (sku) and values of type Inventory (struct)
}
func getInventoryFromGSheet(gSheetURL string) (map[string]Inventory, error) {
	//google sheet integration:
	re := regexp.MustCompile("/spreadsheets/d/([a-zA-Z0-9-_]+)")
	sheet := re.FindAllStringSubmatch(gSheetURL, -1)
	if sheet == nil {
		fmt.Println("Did not find a regular exp match for the sheet!")
	}
	fmt.Println("-----SheetID:", sheet[0][1])
	spreadsheetId := sheet[0][1]

	var resp *sheets.ValueRange
	var err error

	resp, err = gsheetRead(spreadsheetId)
	// fmt.Println("retreieved from Google sheet response=", resp)
	if err != nil {
		fmt.Println("Unable to retrieve data from sheet. %v", err)
	}

	Inv := make(map[string]Inventory)
	var gerr error

	//make inventory from resp
	if len(resp.Values) > 0 {
		for _, record := range resp.Values {
			tmpSKU, ok := record[0].(string)
			fmt.Println("--", tmpSKU, reflect.TypeOf(record[0]).Kind())

			//tmpUPC, _:= strconv.ParseUint(record[1].(string), 10, 64) //UPC
			tmpUPC, ok := record[1].(string)
			tmpUPC2, _ := strconv.ParseUint(tmpUPC, 10, 64)
			fmt.Println("--", tmpUPC2, reflect.TypeOf(record[1]).Kind())

			tmpName, ok := record[2].(string)
			fmt.Println("--", tmpName, reflect.TypeOf(record[2]).Kind())

			tmpDesc, ok := record[3].(string)
			fmt.Println("--", tmpDesc, reflect.TypeOf(record[3]).Kind())

			tmpType, ok := record[4].(string)
			fmt.Println("--", tmpType, reflect.TypeOf(record[4]).Kind())

			//tmpQty, _ := strconv.Atoi(record[5].(string)) //Quantity
			tmpQty, ok := record[5].(string)
			tmpQty2, _ := strconv.Atoi(tmpQty)
			fmt.Println("--", tmpQty, reflect.TypeOf(record[5]).Kind())

			//tmpPrice, _ := strconv.ParseFloat(record[6].(string), 32) //Price
			tmpPrice, ok := record[6].(string)
			tmpPrice2, _ := strconv.ParseFloat(tmpPrice, 32)
			fmt.Println("--", tmpPrice, reflect.TypeOf(record[6]).Kind())

			// tmpShip, _ := strconv.ParseFloat(record[7].(string), 32) //strconv.ParseFloat(record[7], 32)
			tmpShip, ok := record[7].(string) //Shipping
			tmpShip2, _ := strconv.ParseFloat(tmpShip, 32)
			fmt.Println("--", tmpShip, reflect.TypeOf(record[7]).Kind())

			tmpImgloc, ok := record[8].(string)
			fmt.Println("--", tmpImgloc, reflect.TypeOf(record[8]).Kind())

			tmpTags1, ok := record[9].(string)
			fmt.Println("--", tmpTags1, reflect.TypeOf(record[9]).Kind())
			tmpTags2 := strings.Fields(stripchars(tmpTags1, "#")) //Tags
			if !ok {
				fmt.Println("Unable to reflect type correctly from data sheet: err=", err)

				gerr = errors.New("Gsheet reflection error")
				//return nil, gerr
			}

			Inv[tmpSKU] = Inventory{
				tmpSKU,             //SKU
				tmpUPC2,            //upc
				tmpName,            //name
				tmpDesc,            //desc
				tmpType,            //type (category)
				int(tmpQty2),       //Quantity
				float32(tmpPrice2), //Price
				float32(tmpShip2),  //Shipping
				tmpImgloc,          //imageloc  or IMG URLs
				tmpTags2,           //tags
			}
		}
	} else {
		fmt.Print("No data found.")
	}
	return Inv, gerr
}

func getInventoryFromCSV(csvPath string) (map[string]Inventory, error) {
	//fetch inventory from published sheet CSV
	resp, err := http.Get(csvPath)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	r := csv.NewReader(resp.Body)

	//r is a reader on returned response
	itemCount := 1
	Inv := make(map[string]Inventory)
	record, readError := r.Read() //skip header line
	record, readError = r.Read()

	for {
		if readError == io.EOF {
			break
		} else if err != nil {
			return nil, readError
		}

		tmpUPC, _ := strconv.ParseUint(record[1], 10, 64)     //UPC
		tmpQty, _ := strconv.Atoi(record[5])                  //Quantity
		tmpPrice, _ := strconv.ParseFloat(record[6], 32)      //Price
		tmpShip, _ := strconv.ParseFloat(record[7], 32)       //Shipping
		tmpTags := strings.Fields(stripchars(record[9], "#")) //Tags

		//sku is the key
		Inv[record[0]] = Inventory{
			record[0],         //SKU
			tmpUPC,            //upc
			record[2],         //name
			record[3],         //desc
			record[4],         //type (category)
			tmpQty,            //Quantity
			float32(tmpPrice), //Price
			float32(tmpShip),  //Shipping
			record[8],         //imageloc  or IMG URLs
			tmpTags,           //tags
		}
		//fmt.Println("Inventory Item:", record[0], "->", Inv[record[0]])
		record, readError = r.Read()
		itemCount++

	}
	return Inv, nil

}
