package main

import (
	"encoding/json"
	"fmt"
	"github.com/gorilla/securecookie"
	"html/template"
	"io/ioutil"
	"net/http"
	"net/url"
	"os"
	"regexp"
	"strings"
	"time"
)

var hashKey = securecookie.GenerateRandomKey(64)
var blockKey = securecookie.GenerateRandomKey(32)

var cookieHandler = securecookie.New(hashKey, blockKey)

//AdminPage This struct holds the Admin Page items
type AdminPage struct {
	GdriveSheet        string `json:"gdrivesheet"`
	GdriveSheetID      string `json:"gdrivesheetid"`
	ShopURL            string `json:"shopurl"`
	ShopName           string `json:"shopName"`
	SnipCartKey        string `json:"snipcartkey"`
	ShopEmail          string `json:"shopEmail"`
	ShopPhone          string `json:"shopPhone"`
	ShopAbout          string `json:"shopAbout"`
	ShopStatus         int
	ShopTemplate       string `json:"shopTemplate"`
	ShopTemplateList   []string
	ShopCurrency       []string
	GAnalyticsTID      string    `json:"gAnalyticsTid"`
	ShopCreationTime   time.Time `json:"shopCreationTime`
	ShopLastUpdateTime time.Time `json:"shopLastUpdateTime`
}

func (a *AdminPage) save() error {

	//adminPayload := []byte("{ \"shopurl\" :\"" + a.ShopURL + "\", \"shopname\" :\"" + a.ShopName + "\", \"gdrivesheet\" :\"" + a.GdriveSheet + "\", \"snipcartkey\" :\"" + a.SnipCartKey + "\"}")
	var adminPayload []byte
	var err error
	adminPayload, err = json.Marshal(a)

	//First create the directory or folder
	shopDir := "data/recoshins/" + a.GdriveSheetID + "_-_" + a.ShopURL
	err = os.MkdirAll(shopDir, 0711)
	if err != nil {
		fmt.Println("Could not create ", shopDir, " directory to save pages!")
		return err
	}
	fmt.Println("Admin Payload:", string(adminPayload))
	filename := "data/recoshins/" + a.GdriveSheetID + "_-_" + a.ShopURL + "/adminInputs.json"
	err = ioutil.WriteFile(filename, adminPayload, 0600)
	if err != nil {
		fmt.Println("Could not save Admin Page details to Admin.TXT")
		return err
	}
	fmt.Println("Saved Admin Page details to adminInputs.json")
	return nil
}

func (a *AdminPage) load() error {
	// filename := "data/recoshins/" + a.ShopURL + "/adminInputs.json"
	if a.ShopURL == "" {
		shopDirList := getShopDataDirMap()
		fmt.Println("--Retrieved the shop dirnames:")
		a.ShopURL = shopDirList[a.GdriveSheetID]
	}
	filename := "data/recoshins/" + a.GdriveSheetID + "_-_" + a.ShopURL + "/adminInputs.json"
	adminPayload, err := ioutil.ReadFile(filename)
	if err != nil {
		fmt.Println("Error reading admin data file", err.Error())
	} else {
		err = json.Unmarshal(adminPayload, a)
		if err != nil {
			fmt.Println("Error parsing admin data file", err.Error())
		}

	}
	fmt.Println("Loaded Admin Data->", a)
	return err
}

func adminHandler(w http.ResponseWriter, r *http.Request) {
	//shopURL := r.URL.Path[len("/admin/"):]
	//fmt.Println("Shop r.URL:", r.URL, "Shop shopURL:", shopURL)

	isRedirected := !strings.HasPrefix(strings.ToLower(r.URL.Path), "/admin")

	if !isRedirected {

		gdriveSheetIDorURL := r.FormValue("element_1")
		gdriveSheetID := getGooglesheetID(gdriveSheetIDorURL)

		if gdriveSheetID != "" {
			gdriveSheetURL := "https://docs.google.com/spreadsheets/d/" + gdriveSheetID
			fmt.Println("Shop gdriveSheetURL:", gdriveSheetURL)
			a := &AdminPage{ShopURL: "", ShopName: "", GdriveSheet: gdriveSheetURL, GdriveSheetID: gdriveSheetID, SnipCartKey: "", ShopEmail: "", ShopPhone: "", ShopAbout: "", ShopTemplate: "", ShopStatus: SHOP_ON}
			a.load()
			a.ShopTemplateList = getTemplateList()
			t, _ := template.ParseFiles("templates/private/layout/admin.tmpl")
			t.Execute(w, a)
		} else {
			//TODO: Return an error message
			fmt.Println("Incorrect google sheet id in login")
			redirectURL := "/"
			fmt.Println("***RedirectURL:", redirectURL)
			http.Redirect(w, r, redirectURL, http.StatusFound)
		}
	} else {

		splitValues := strings.Split(r.URL.Path, "/")
		fmt.Println(splitValues)
		fmt.Println(len(splitValues))
		splitValues = strings.Split(splitValues[1], "_-_")
		fmt.Println(splitValues)
		fmt.Println(len(splitValues))
		gdriveSheetID := splitValues[0]
		shopURL := splitValues[1]

		fmt.Println("Google sheet id" + gdriveSheetID)
		fmt.Println("shopURL" + shopURL)

		if gdriveSheetID == "" {
			fmt.Println("Incorrect google sheet id in url")
			redirectURL := "/"
			fmt.Println("***RedirectURL:", redirectURL)
			http.Redirect(w, r, redirectURL, http.StatusFound)
		}

		gdriveSheetURL := "https://docs.google.com/spreadsheets/d/" + gdriveSheetID
		fmt.Println("Shop gdriveSheetURL:", gdriveSheetURL)
		a := &AdminPage{ShopURL: shopURL, ShopName: "", GdriveSheet: gdriveSheetURL, GdriveSheetID: gdriveSheetID, SnipCartKey: "", ShopEmail: "", ShopPhone: "", ShopAbout: "", ShopTemplate: "", ShopStatus: SHOP_ON}
		a.load()
		a.ShopTemplateList = getTemplateList()
		t, _ := template.ParseFiles("templates/private/layout/admin.tmpl")
		t.Execute(w, a)
	}
}

func getGooglesheetID(urlString string) string {
	isUrl := false

	if strings.Trim(urlString, " ") == "" {
		return ""
	}

	u, err := url.Parse(urlString)
	if err != nil {
		isUrl = false
	} else {
		isUrl = true
	}

	isUrl = isUrl &&
		strings.ToLower(u.Host) == "docs.google.com" &&
		strings.HasPrefix(strings.ToLower(u.Path), "/spreadsheets/d/")

	if isUrl {
		urlString = strings.Replace(urlString, "/SPREADSHEETS/", "/spreadsheets/", -1)
		urlString = strings.Replace(urlString, "/D/", "/d/", -1)
		re := regexp.MustCompile("/spreadsheets/d/([a-zA-Z0-9-_]+)")
		sheet := re.FindAllStringSubmatch(urlString, -1)
		fmt.Println(sheet)
		if len(sheet) > 0 {
			for _, h := range sheet {
				if len(h) > 1 {
					return sheet[0][1]
				}
			}
		}

		return ""
	} else {

		re := regexp.MustCompile("([a-zA-Z0-9-_]+)")
		fmt.Println(urlString)
		sheet := re.FindAllStringSubmatch(urlString, -1)
		fmt.Println(sheet)
		if len(sheet) > 0 {
			for _, h := range sheet {
				if len(h) > 0 {
					return sheet[0][0]
				}
			}
		}

		return ""
	}

}

func getTemplateList() []string {
	files, _ := ioutil.ReadDir("./templates/recoshins")
	var templateList []string
	i := 0
	fmt.Println(files)
	for _, v := range files {
		templateList = append(templateList, v.Name())
		i++
	}

	return templateList
}

func getShopDataDirMap() map[string]string {
	dirs, _ := ioutil.ReadDir("./data/recoshins/")
	var shopDataDirMap map[string]string
	shopDataDirMap = make(map[string]string)
	fmt.Println(dirs)
	for _, v := range dirs {
		if v.IsDir() {
			dir := v.Name()
			fmt.Println("Dir Name: ", dir)
			temp := strings.Split(dir, "_-_")
			if len(temp) == 2 {
				shopDataDirMap[temp[0]] = temp[1]
			}

		}
		fmt.Println("shopDataDirMap:", shopDataDirMap)
	}

	return shopDataDirMap
}

func submitHandler(w http.ResponseWriter, r *http.Request) {
	//title := r.URL.Path[len("/save/"):]
	var shopCreationTime, shopLastUpdateTime time.Time
	var saveda AdminPage //load the current admin data from json for comparision with values returned in the form

	redirectURL := "/admin/"
	shopURL := r.FormValue("element_1")
	gdriveSheetURL := r.FormValue("element_2")
	shopName := r.FormValue("element_3")
	snipCartKey := r.FormValue("element_4")
	shopPhone := r.FormValue("element_5")
	shopEmail := r.FormValue("element_6")
	shopTemplateName := r.FormValue("element_7")
	shopAbout := r.FormValue("element_8")
	gAnalyticsTid := r.FormValue("element_9")

	saveda.ShopURL = shopURL
	saveda.load()

	fmt.Println("----saved A:", saveda.ShopCreationTime, saveda.ShopLastUpdateTime)
	shopCreationTime = saveda.ShopCreationTime
	shopLastUpdateTime = saveda.ShopLastUpdateTime
	if saveda.ShopCreationTime.IsZero() {
		shopCreationTime = time.Now()
		// fmt.Println("--time:", shopCreationTime)
		if saveda.ShopLastUpdateTime.IsZero() {
			shopLastUpdateTime = time.Now()
		}
	}
	// if !!saveda.ShopLastUpdateTime.IsZero() {
	// 	shopLastUpdateTime = time.Now()
	// }

	fmt.Println("Submitted-> Shop URL: ", shopURL+
		" Shop Name: ", shopName+
		" Gdrive Sheet URL:", gdriveSheetURL+
		" SnipCart API Key:", snipCartKey+
		" Shop Update Time:", shopLastUpdateTime)

	gdriveSheetID := getGooglesheetID(gdriveSheetURL)

	if gdriveSheetID == "" {
		fmt.Println("Did not get the spreadsheet id to save!")
		return
	}

	a := &AdminPage{ShopURL: shopURL,
		ShopName:           shopName,
		GdriveSheet:        gdriveSheetURL,
		GdriveSheetID:      gdriveSheetID,
		SnipCartKey:        snipCartKey,
		ShopPhone:          shopPhone,
		ShopTemplate:       shopTemplateName,
		ShopEmail:          shopEmail,
		ShopAbout:          shopAbout,
		GAnalyticsTID:      gAnalyticsTid,
		ShopCreationTime:   shopCreationTime,
		ShopLastUpdateTime: shopLastUpdateTime}

	if createStaticRecoshin(gdriveSheetURL, *a) == nil {
		fmt.Println("Creation of Static pages has begun!")
		a.ShopLastUpdateTime = time.Now()
		fmt.Println("--New update time:", a.ShopLastUpdateTime)

	} else {
		fmt.Println("Failed in creating static pages!!")
	}

	//save the new admin form only if the creation of static pages has succeeded
	if a.save() != nil {
		fmt.Println("Failed saving the shop admin data to file!")
	}

	// ---- if we use the ghseetid then we dont have to do a login.
	//the gsheetid is unique for a shop and only the seller has that sheet and its access
	//we shall change the data/recoshins/<shopname> to data/recoshins/<gsheetid>
	//Also we shall change the access from /admin/<shopname> to /admin/<ghseetid>or<gsheeturl>

	//To do: come up with a way to validate that there are not multiple shops with same email id
	//as we want to restrict the unnecessary usage of our service
	//since we dont use a DB looking for an email in multiple files could be expensive
	//one way would be to concatenate both email and gsheetid and then name the data/recoshins/<email+gsheetid> file

	//If cookie is set
	// check if cookie is set with a shop and email id  --- or ---- gsheet id?
	//	if shop and email is different from cookie's then dont submit  raise error
	//	if shop and email is same as in cookie then submit
	//		show logout button
	//If cookie is not set
	// check if shop and email combo is unique/new
	//	if unique set cookie and submit
	//	if not unique dont submit raise error

	rcookie, err := r.Cookie("recoshin") //read request cookie if already set
	if err != nil {
		//Set a cookie
		value := map[string]string{
			"shopURL": shopURL,
		}
		encoded, err := cookieHandler.Encode("recoshin", value)
		if err == nil {
			cookie := &http.Cookie{
				Name:  "recoshin",
				Value: encoded,
				Path:  "/",
			}
			fmt.Println("------------ Response cookie value", encoded)
			http.SetCookie(w, cookie)
		}
	} else {
		fmt.Println("---------------   Request Cookie Value:", rcookie.Value)
	}

	if shopURL != "" {
		redirectURL = "/" + gdriveSheetID + "_-_" + shopURL + "/admin"
		fmt.Println("***RedirectURL:", redirectURL)
		//TESTING
		http.HandleFunc(redirectURL, adminHandler)

	}
	http.Redirect(w, r, redirectURL, http.StatusFound)

}
